﻿using BulkyBook.DataAccess;
using BulkyBook.DataAccess.Repository;
using BulkyBook.DataAccess.Repository.IRepository;
using BulkyBook.Models;
using BulkyBook.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.DotNet.Scaffolding.Shared.Messaging;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace BulkyBookWeb.Areas.Admin.Controllers;
[Area("Admin")]
public class CompanyController : Controller
{
    private readonly IUnitOfWork _unitofwork;
    

    public CompanyController(IUnitOfWork unitofwork)
    {
        _unitofwork = unitofwork;
        
    }

    public IActionResult Index()
    {
        

        //IEnumerable<Product> objProductList = _unitofwork.Product.GetAll();
        return View();
    }
    
    ///////// Change  public IActionResult Edit , as
    //GET
    public IActionResult Upsert(int? id)
    {
          Company  company = new();
           

        if (id == null || id == 0)
        {
            // Create Product


            //ViewBag.CategoryList = CategoryList;
            //ViewData["CoverTypeList"] = CoverTypeList;
            //return View(product);
            return View(company);
        }
        else
        {
            company = _unitofwork.Company.GetFirstOrDefault(u => u.Id == id);
            // update Product
            return View(company);
        }

        
    }

    //POST
    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Upsert(Company obj, IFormFile? file)
    {
        if (ModelState.IsValid)
        {            

            if(obj.Id == 0)
            {
                _unitofwork.Company.Add(obj);
                TempData["success"] = "Company added Successfully";
            }
            else
            {
                _unitofwork.Company.Update(obj);
                TempData["success"] = "Company updated Successfully";
            }
            
            _unitofwork.Save();
            
            return RedirectToAction("Index");
        }
        return View(obj);
    }

    


    #region APICalls
    [HttpGet]
    public IActionResult GetAll() 
    {
        var CompanyList = _unitofwork.Company.GetAll();
        return Json(new {data = CompanyList});
    }

    //POST
    [HttpDelete]
    //[ValidateAntiForgeryToken]
    public IActionResult Delete(int? id)
    {
        var obj = _unitofwork.Company.GetFirstOrDefault(u => u.Id == id);
        if (obj == null)
        {
            return Json(new { success = false, message = "Error while deleting!"});
        }
       
        _unitofwork.Company.Remove(obj);
        _unitofwork.Save();
        
        return Json(new { success = true, message = "Deleted successfully"});
        
    }
    #endregion
}
